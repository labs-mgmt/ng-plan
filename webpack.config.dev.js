const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');

const commonConfig = require('./webpack.config.js');

module.exports = merge(commonConfig, {
    mode: 'development',
    devtool: 'eval-source-map',
    watchOptions: {
        ignored: /node_modules/,
        poll: 1000, // Check for changes every second
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(__dirname, 'public/index.html'),
            favicon: './public/favicon.ico',
        }),
    ],
});
