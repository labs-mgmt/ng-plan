import _ from 'lodash';
import { FilterMatchMode } from 'primevue/api';
import { onMounted, ref, watch } from 'vue';
import { useRoute } from 'vue-router';

import { type ColumnFields } from '../components/consts';

type Filter = {
    [key in ColumnFields | 'global']: {
        value?: number[] | boolean | null;
        matchMode?: string;
    };
};

const diffFilters = (
    defaultValues: Filter,
    currentValues: Filter,
): Partial<Filter> => {
    const differences: Partial<Filter> = {};

    for (const key in defaultValues) {
        const defaultValue = defaultValues[key].value;
        const currentValue = currentValues[key]?.value;

        if (
            Array.isArray(defaultValue) &&
            _.difference(defaultValue, currentValue).length === 0
        ) {
            continue;
        }

        if (defaultValue !== currentValue) {
            differences[key] = {
                value: currentValue,
                matchMode: currentValues[key]?.matchMode,
            };
        }
    }

    return differences;
};

export const DEFAULT_FILTER_VALUES = Object.freeze({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    iid: { value: [0, 1000], matchMode: FilterMatchMode.BETWEEN },
    project: { value: null, matchMode: FilterMatchMode.IN },
    part: { value: null, matchMode: FilterMatchMode.CONTAINS },
    description: { value: null, matchMode: FilterMatchMode.CONTAINS },
    quarter: { value: null, matchMode: FilterMatchMode.IN },
    personDays: { value: null, matchMode: FilterMatchMode.BETWEEN },
    responsible: { value: null, matchMode: FilterMatchMode.IN },
    submitter: { value: null, matchMode: FilterMatchMode.IN },
    internal: { value: null, matchMode: '01_EQUAL' },
    note: { value: null, matchMode: FilterMatchMode.CONTAINS },
    doneLvl: { value: null, matchMode: FilterMatchMode.IN },
    bonus: { value: null, matchMode: '01_EQUAL' },
    leader: { value: null, matchMode: FilterMatchMode.CONTAINS },
    'assignee.name': { value: null, matchMode: FilterMatchMode.CONTAINS },
});

export const useFilters = () => {
    const filters = ref<Filter>({ ...DEFAULT_FILTER_VALUES });

    const handleFilterChange = () => {
        console.log(filters.value);
        const filtered = diffFilters(DEFAULT_FILTER_VALUES, filters.value);
        const queryParams = Object.keys(filtered).map((key) => {
            // It can be any key all are the same
            const value = filtered[key] as Filter['description'];
            return [key, value.value];
        });

        const params = new URLSearchParams();
        for (const param of queryParams) {
            params.set(param[0] as string, param[1] as string);
        }
        const stringifiedParams = params.toString();
        window.history.pushState(
            {},
            '',
            `${window.location.pathname}${
                stringifiedParams.length > 0 ? '?' : ''
            }${stringifiedParams}`,
        );
    };

    watch(filters, handleFilterChange);

    const route = useRoute();

    const handleValue = (boolStr: string) => {
        const isBool = ['true', 'false'].includes(boolStr);
        if (isBool) {
            return boolStr === 'true';
        }
        return boolStr.split(',');
    };

    onMounted(() => {
        const myQuery = route.query as Record<string, string>;
        const newFilter: Partial<Filter> = Object.keys(myQuery)
            .map((key) => ({
                [key]: {
                    value: handleValue(myQuery[key]),
                    matchMode: filters.value[key].matchMode,
                },
            }))
            .reduce((acc, val) => ({ ...acc, ...val }), {});

        filters.value = { ...filters.value, ...newFilter };
    });

    return { filters };
};
