export type DepartmentName =
    | 'admins'
    | 'csirt'
    | 'akademie'
    | 'fin'
    | 'labs'
    | 'mkt'
    | 'techdep'
    | 'turris'
    | 'sales';

export interface Department {
    id: string;
    name: DepartmentName;
    token: string;
}

const DEPARTMENTS: Readonly<Department[]> = [
    {
        id: '1091',
        name: 'admins',
        token: '-Fx3xuS1zXwm1N5Equps',
    },
    {
        id: '1214',
        name: 'akademie',
        token: 'glpat-3x8se5ZQtjCjDxxyo9jD',
    },
    {
        id: '1212',
        name: 'csirt',
        token: 'glpat-FHpaDtWovzbrEiyhrW2h',
    },
    {
        id: '1176',
        name: 'fin',
        token: 'glpat-WWt97HySa-gaZvjHjtJx',
    },
    {
        id: '1218',
        name: 'labs',
        token: 'glpat--ZxbNPtv8T6___Fk1Tsi',
    },
    {
        id: '1177',
        name: 'mkt',
        token: 'glpat-M24KZ-kMFUvoUFvsvm78',
    },
    {
        id: '1376',
        name: 'sales',
        token: 'glpat-5rhQCbyxQGHQmmFaCi8q',
    },
    {
        id: '1072',
        name: 'techdep',
        token: 'jzTwgNNvk1sdkAtGrQAd',
    },
    {
        id: '1175',
        name: 'turris',
        token: 'glpat-wELyPHvUB9s_fE4CQTsw',
    },
];

export default DEPARTMENTS;
