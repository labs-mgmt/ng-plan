import { type AxiosRequestConfig } from 'axios';

import axiosInstance from '../plugins/axios';

import type { Department } from './departments';

interface IssueLabel {
    name: string;
}

interface IssueList {
    label: IssueLabel;
}

interface Milestone {
    start_date: string;
    due_date: string;
    title: string;
}

interface Assignee {
    avatar_url: string;
    id: number;
    name: string;
    state: string;
    username: string;
    web_url: string;
}

interface Issue {
    assignee: Assignee;
    iid: number;
    user_notes_count: number;
    title: string;
    description: string;
    state: string;
    labels: string[];
    milestone: Milestone;
    web_url: string;
    created_at: string;
}

interface Note {
    body: string;
    system: boolean;
}

export enum DoneLevel {
    UNDONE = '0',
    DONE = '1',
    FREEZED = '2',
}

export interface Task {
    iid: number;
    webUrl: string;
    hasStatus: boolean;
    project: string;
    leader: string;
    assignee: Assignee | string;
    part: string;
    description: string;
    quarter: string;
    personDays: number;
    submitter: string;
    done: boolean;
    responsible: string;
    note: string;
    internal: 0 | 1;
    bonus: 0 | 1;
    added: boolean;
    freezed: boolean;
    doneLvl: DoneLevel;
    milestoneDeadlineYear: number | null;
}

const taskDefaults: Pick<
    Task,
    'internal' | 'bonus' | 'freezed' | 'note' | 'added' | 'doneLvl'
> = {
    internal: 0,
    bonus: 0,
    freezed: false,
    note: '',
    added: false,
    doneLvl: DoneLevel.UNDONE,
};

export interface Project {
    webUrl: string;
}

const fetchProjects = async (
    projectId: string,
    config: AxiosRequestConfig,
): Promise<string[]> => {
    try {
        const { data } = await axiosInstance.get(`${projectId}/boards`, config);

        const issueLists = data.map(
            (issueBoard: { lists: IssueList[] }) => issueBoard.lists,
        )[0];

        return issueLists.map(
            (issueListItem: IssueList) => issueListItem.label.name,
        );
    } catch (e) {
        console.error('Could not fetch the projects');
    }
    return [];
};

export const getYears = async (
    projectId: string,
    config: AxiosRequestConfig,
): Promise<Set<number> | null> => {
    try {
        const { data } = await axiosInstance.get(
            `${projectId}/milestones`,
            config,
        );
        const fullYear = data.map((milestone: Milestone) =>
            new Date(milestone.due_date).getFullYear(),
        );
        return new Set<number>(fullYear);
    } catch (e) {
        console.error('Could not fetch the departments');
    }
    return null;
};

const getStatus = async (
    issueId: number,
    projectId: string,
    config: AxiosRequestConfig,
): Promise<string> => {
    try {
        const { data } = await axiosInstance.get(
            `${projectId}/issues/${issueId}/notes`,
            config,
        );
        const notes = data.filter((note: Note) => !note.system);
        return notes?.length > 0 ? notes[0].body : '';
    } catch (e) {
        console.info(`Status for Issue #${issueId} is missing`);
    }
    return '';
};

const mapIssue = async (
    issue: Issue,
    department: string,
    project: string,
    projectId: string,
    config: AxiosRequestConfig,
): Promise<Task> => {
    const keyValueRegex = /(\S*)\s*:\s*(.*)/;
    let part = '';
    let personDays = 0;
    let leader = '';
    let submitter = '';

    const assignee = issue.assignee ?? { name: '' };
    const quarter = issue.milestone?.title.split('/')[1];

    issue.description.split('\n').forEach((line) => {
        const matchResult = line.match(keyValueRegex);
        if (matchResult !== null) {
            const [, key, value] = matchResult;
            switch (key) {
                case 'Část':
                    part = value;
                    break;
                case 'Zadavatel':
                    submitter = value;
                    break;
                case 'Člověkodny':
                    personDays = +value;
                    break;
                case 'Tahoun':
                    leader = value;
                    break;
            }
        }
    });

    const res: Task = {
        ...taskDefaults,
        iid: issue.iid,
        webUrl: issue.web_url,
        hasStatus: issue.user_notes_count > 0,
        project,
        leader,
        assignee,
        part,
        description: issue.title,
        quarter,
        personDays,
        submitter,
        done: issue.state === 'closed',
        responsible: department.toUpperCase(),
        // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
        milestoneDeadlineYear: issue?.milestone?.due_date
            ? new Date(issue.milestone.due_date).getFullYear()
            : null,
    };

    issue.labels.forEach((label) => {
        switch (label) {
            case 'interní':
                res.internal = 1;
                break;
            case 'bonus':
                res.bonus = 1;
                break;
            case 'přidán':
                res.added = true;
                break;
            case 'freezed':
                res.freezed = true;
        }

        if (res.freezed) {
            res.doneLvl = DoneLevel.FREEZED;
        } else if (res.done) {
            res.doneLvl = DoneLevel.DONE;
        } else {
            res.doneLvl = DoneLevel.UNDONE;
        }
    });

    res.note =
        res.doneLvl === DoneLevel.DONE
            ? ''
            : await getStatus(issue.iid, projectId, config);

    return res;
};

const getNextPageUrl = (
    labels: string,
    linkHeader: string | undefined,
): string | null => {
    if (linkHeader === undefined || linkHeader === '') return null;

    const links = linkHeader
        .split(',')
        .map((part) => part.split(';').map((p) => p.trim()));
    const nextLink = links.find((part) => part[1] === 'rel="next"');

    // has next link
    if (nextLink != null) {
        // Remove < and >
        const parsedURL = new URL(nextLink[0].slice(1, -1));

        // Get all search params
        const searchParams = new URLSearchParams(parsedURL.search);
        const sortBy = searchParams.get('sort');
        const perPage = searchParams.get('per_page');
        const page = searchParams.get('page');
        // Build new URL
        const pageParam = page !== null ? `&page=${page}` : '';
        const params = `labels=${labels}&sort=${sortBy ?? 'asc'}&per_page=${
            perPage ?? '100'
        }${pageParam}`;
        const newURL = `${parsedURL.origin}${parsedURL.pathname}?${params}`;
        return newURL;
    }

    return null;
};

const fetchAllPages = async (
    startURL: string,
    label: string,
    project: string,
    department: string,
    projectId: string,
    config: AxiosRequestConfig,
) => {
    let allTasks: Task[] = [];
    let nextPageUrl: string | null = startURL;

    while (nextPageUrl !== null && nextPageUrl !== '') {
        const response = await axiosInstance.get<Issue[]>(nextPageUrl, config);
        const tasks = await Promise.all(
            response.data.map(
                async (issue) =>
                    await mapIssue(
                        issue,
                        department,
                        project,
                        projectId,
                        config,
                    ),
            ),
        );
        allTasks = allTasks.concat(tasks);

        const linkHeader = response.headers.link;
        nextPageUrl = getNextPageUrl(label, linkHeader);
    }
    return allTasks;
};

const fetchIssues = async (
    project: string,
    department: string,
    projectId: string,
    config: AxiosRequestConfig,
): Promise<Task[]> => {
    const encodedProjectLabel = encodeURIComponent(project);
    // There is a limit inside gitlab (MAX 100 per page)
    const url = `${projectId}/issues?labels=${encodedProjectLabel}&sort=asc&per_page=100`;

    try {
        return await fetchAllPages(
            url,
            encodedProjectLabel,
            project,
            department,
            projectId,
            config,
        );
    } catch (e) {
        console.error('Error fetching issues:', e);
        console.info('Missing tasks for', project);
    }

    return [];
};
export const fetchProjectInfo = async (
    department: Department,
): Promise<Project> => {
    // fetch projects
    const { data } = await axiosInstance.get(department.id, {
        headers: { 'Private-Token': department.token },
    });

    return {
        webUrl: data.web_url,
    };
};

export const getIssues = async (
    department: Department,
): Promise<[Task[], number[]]> => {
    const config = {
        headers: { 'Private-Token': department.token },
    };

    const projects = await fetchProjects(department.id, config);
    const getAllIssues = async () => {
        let allIssues: Task[] = [];
        for (const project of projects) {
            const issue = await fetchIssues(
                project,
                department.name,
                department.id,
                config,
            );
            allIssues = [...allIssues, ...issue];
        }
        return allIssues;
    };

    const [issues, years] = await Promise.all([
        getAllIssues(),
        getYears(department.id, config),
    ]);
    const sortedYears = [...(years ?? [])].sort((a, b) => a - b);
    return [issues, sortedYears];
};
