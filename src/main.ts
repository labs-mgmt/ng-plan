import { VueQueryPlugin } from '@tanstack/vue-query';
import Button from 'primevue/button';
import Column from 'primevue/column';
import PrimeVue from 'primevue/config';
import DataTable from 'primevue/datatable';
import Dropdown from 'primevue/dropdown';
import InputText from 'primevue/inputtext';
import MultiSelect from 'primevue/multiselect';
import Slider from 'primevue/slider';
import Toolbar from 'primevue/toolbar';
import Tooltip from 'primevue/tooltip';
import TriStateCheckbox from 'primevue/tristatecheckbox';
import { createApp } from 'vue';

import cs from './i18n/cs';
import router from './router';

import { App } from '.';
import 'primevue/resources/themes/tailwind-light/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.min.css';

const app = createApp(App);

app.use(router);
app.use(VueQueryPlugin);
app.use(PrimeVue, {
    locale: cs,
});

app.component('PrimeDataTable', DataTable);
app.component('PrimeDropdown', Dropdown);
app.component('PrimeColumn', Column);
app.component('PrimeSlider', Slider);
app.component('PrimeMultiSelect', MultiSelect);
app.component('PrimeInputText', InputText);
app.component('PrimeButton', Button);
app.component('PrimeTriStateCheckbox', TriStateCheckbox);
app.component('PrimeToolbar', Toolbar);

app.directive('tooltip', Tooltip);

app.mount('#app');
