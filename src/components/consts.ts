export type ColumnFields =
    | 'iid'
    | 'project'
    | 'part'
    | 'description'
    | 'quarter'
    | 'personDays'
    | 'responsible'
    | 'submitter'
    | 'internal'
    | 'note'
    | 'doneLvl'
    | 'bonus'
    | 'leader'
    | 'assignee.name';

export type ColumnNames = Exclude<ColumnFields, 'doneLvl'> | 'done';

export interface Columns {
    name: ColumnNames;
    field?: ColumnFields;
    label?: string;
    align?: string;
    sortable?: boolean;
    style?: string;
    required?: boolean;
}

export const COLUMNS = [
    {
        name: 'iid',
        field: 'iid',
        label: 'iid',
        align: 'left',
        required: true,
        sortable: true,
    },
    {
        name: 'Project',
        field: 'project',
        label: 'Oblast',
        align: 'left',
        sortable: true,
        required: true,
    },
    {
        name: 'part',
        field: 'part',
        label: 'Část',
        align: 'left',
        sortable: true,
        required: true,
    },
    {
        name: 'description',
        field: 'description',
        label: 'Popis',
        align: 'left',
        sortable: true,
        required: true,
    },
    {
        name: 'q',
        field: 'quarter',
        label: 'Q',
        align: 'left',
        required: true,
        sortable: true,
    },
    {
        name: 'personDays',
        field: 'personDays',
        label: 'ČD',
        align: 'left',
        sortable: true,
        required: true,
    },
    {
        name: 'responsible',
        field: 'responsible',
        label: 'Odpov.',
        align: 'left',
        sortable: true,
    },
    {
        name: 'submitter',
        field: 'submitter',
        label: 'Zadavatel',
        align: 'left',
        sortable: true,
    },
    {
        name: 'internal',
        field: 'internal',
        label: 'Interní',
        align: 'center',
        sortable: true,
    },
    {
        name: 'note',
        field: 'note',
        label: 'Poznámka',
        align: 'left',
        sortable: true,
        style: '',
    },
    {
        name: 'done',
        field: 'doneLvl',
        label: 'Hotovo',
        align: 'center',
        sortable: true,
    },
    {
        name: 'bonus',
        field: 'bonus',
        label: 'Bonus',
        align: 'center',
        sortable: true,
    },
    {
        name: 'leader',
        field: 'leader',
        label: 'Tahoun',
        align: 'center',
        sortable: true,
        style: '',
    },
    {
        name: 'assignee',
        field: 'assignee',
        label: 'Řešitel',
        align: 'left',
        sortable: true,
    },
];
