import type { RouterOptions } from 'vue-router';

import DEPARTMENTS from '../client/departments';
import { MainPage, NotFound } from '../components';

const filterByName = (departments, depName) =>
    departments.filter((department) => department.name === depName);

export const routes: RouterOptions['routes'] = [
    {
        path: '',
        redirect: 'all',
    },
    {
        name: 'all',
        path: '/all',
        component: MainPage,
        props: { department: DEPARTMENTS },
    },
    {
        name: 'admins',
        path: '/admins',
        component: MainPage,
        props: {
            department: filterByName(DEPARTMENTS, 'admins'),
        },
    },
    {
        name: 'akademie',
        path: '/akademie',
        component: MainPage,
        props: {
            department: filterByName(DEPARTMENTS, 'akademie'),
        },
    },
    {
        name: 'csirt',
        path: '/csirt',
        component: MainPage,
        props: { department: filterByName(DEPARTMENTS, 'csirt') },
    },
    {
        name: 'fin',
        path: '/fin',
        component: MainPage,
        props: { department: filterByName(DEPARTMENTS, 'fin') },
    },
    {
        name: 'labs',
        path: '/labs',
        component: MainPage,
        props: { department: filterByName(DEPARTMENTS, 'labs') },
    },
    {
        name: 'mkt',
        path: '/mkt',
        component: MainPage,
        props: { department: filterByName(DEPARTMENTS, 'mkt') },
    },
    {
        name: 'sales',
        path: '/sales',
        component: MainPage,
        props: { department: filterByName(DEPARTMENTS, 'sales') },
    },
    {
        name: 'techdep',
        path: '/techdep',
        component: MainPage,
        props: { department: filterByName(DEPARTMENTS, 'techdep') },
    },
    {
        name: 'turris',
        path: '/turris',
        component: MainPage,
        props: { department: filterByName(DEPARTMENTS, 'turris') },
    },
    {
        name: 'not-found',
        path: '/:pathMatch(.*)*',
        component: NotFound,
    },
];
