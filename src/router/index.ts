import { createRouter, createWebHistory } from 'vue-router';

import { routes } from './routes';
const router = createRouter({
    history: createWebHistory('.'),
    routes,
});

router.beforeEach((to, _, next) => {
    const routeTitle = to.name?.toString() as string;
    document.title = `${routeTitle.toUpperCase()} - Plán | CZ.NIC`;
    next();
});

export default router;
