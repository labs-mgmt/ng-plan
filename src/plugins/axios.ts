import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://gitlab.office.nic.cz/api/v4/projects/',
});

export default instance;
